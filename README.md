# Word Frequency Project

Los requerimientos estan basados por Florin Pop en el siguiente [link](https://github.com/florinpop17/app-ideas/blob/master/Projects/1-Beginner/Word-Frequency-App.md)

Actualmente el proyecto cuenta con las caracteristicas de User Stories

## Tecnologias utilizadas

* [Laravel](https://laravel.com/docs/8.x) - Back-end Framework
* [Vue JS](https://vuejs.org/v2/guide/) - Front-end Framework
* [Bootstrap](https://getbootstrap.com/docs/5.0/getting-started/introduction/) - CSS Framework

## Instalación

### Clonar el repositorio

```
git clone https://gitlab.com/federicko94/word-frequency.git
```

### Instalar las dependencias de Laravel
Ingresar a la carpeta del proyecto y realizar lo siguiente
```
composer install
npm install
```

### Generar el archivo .env

* **Windows**


```
copy .env.example .env
```

* **Linux**

```
cp .env.example .env
```
### Generar el entorno del proyecto

```
php artisan key:generate
```

### Modificar el archivo .env

```
DB_CONNECTION= {Driver de la BD}
DB_HOST={Direccion IP de la BD}
DB_PORT={Puerto de la BD}
DB_DATABASE={Nombre de la BD}
DB_USERNAME={Usuario de la BD}
DB_PASSWORD={Contraseña de la BD}
```
### Ejecutar las migraciones

```
php artisan migrate
```

### Crear los archivos JS y CSS

```
npm run dev
```

### Levantar el Proyecto

```
php artisan serve
Ingresar al link http://127.0.0.1:8000/
```
