<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Word;
use App\Models\Frequency;

class WordsFrequencyController extends Controller
{
    public function index()
    {
        $words = Word::all();
        return $words;
    }
    public function store(Request $request)
    {

        $pattern = '/[^a-zA-Z0-9\s]/';
        $str = $request->textarea;
        $str_clear = preg_replace($pattern, '', $str);
        
        $words = explode(' ', $str_clear);
        $frequency = array();

        /*recorrer el array de palabras, si la palabra es nueva se crea el indice con el 
        valor 0, sino continua acumulandosé*/   
        foreach ($words as $word) {
            if(!empty($word)){
                if (!isset($frequency[$word])) {
                    $frequency[$word] = 0;
                }
                $frequency[$word]++;
            }
        }
        /*recorrer el array de frecuencias, si existe en la bd se modifica el valor 
        count(sumando el valor de la bd + el actual), si no se crea un nuevo registro*/
        foreach ($frequency as $key => $value) {
            $word = Word::where('word', 'like', $key )->firstOrNew();
            if($word->id){
                $word->count = $word->count + $value;
                $word->update();
            }else{
                $word->word = $key;
                $word->count = $value;
                $word->save();
            }
            echo($word);
        }

    }
    public function destroy($id){
        Word::destroy($id);
    }
}
