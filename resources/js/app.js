
require('./bootstrap');
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

window.Vue = require('vue');

//COMPONENTS
Vue.component('words-frequency-component', require('./components/WordsFrequencyComponent.vue').default);
Vue.component('input-words-component', require('./components/InputWordsComponent.vue').default);
Vue.component('table-words-component', require('./components/TableWordsComponent.vue').default);
Vue.component('graphic-words-component', require('./components/GraphicWordsComponent.vue').default);


const app = new Vue({
    el: '#app',
});
